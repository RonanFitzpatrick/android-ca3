package com.android.ca3.roster;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.api.client.util.DateTime;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;

/**
 * Created by Ronan on 07/12/2016.
 */

public class CalEvent implements Parcelable
{

    private String eventName;
    private DateTime date;
    private ArrayList<String> attendees;
    private String attachmentUrl;
    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public CalEvent(String attachmentUrl, String eventName, DateTime date, String id) {
        this.attachmentUrl = attachmentUrl;
        this.eventName = eventName;
        this.date = date;
        this.id = id;
    }

    public DateTime getDate() {
        return date;
    }

    public void setDate(DateTime date) {
        this.date = date;
    }

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public ArrayList<String> getAttendees() {
        return attendees;
    }

    public void setAttendees(ArrayList<String> attendees) {
        this.attendees = attendees;
    }

    public String getAttachmentUrl() {
        return attachmentUrl;
    }

    public void setAttachmentUrl(String attachmentUrl) {
        this.attachmentUrl = attachmentUrl;
    }

    @Override
    public int describeContents()
    {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags)
    {
        dest.writeString(this.eventName);
        dest.writeStringList(this.attendees);
        dest.writeString(this.attachmentUrl);
        dest.writeString(this.id);
    }

    protected CalEvent(Parcel in)
    {
        this.eventName = in.readString();
        this.attendees = in.createStringArrayList();
        this.attachmentUrl = in.readString();
        this.id = in.readString();
    }

    public static final Parcelable.Creator<CalEvent> CREATOR = new Parcelable.Creator<CalEvent>()
    {
        @Override
        public CalEvent createFromParcel(Parcel source)
        {
            return new CalEvent(source);
        }

        @Override
        public CalEvent[] newArray(int size)
        {
            return new CalEvent[size];
        }
    };
}
