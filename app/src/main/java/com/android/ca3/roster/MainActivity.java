package com.android.ca3.roster;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential;
import com.google.api.client.googleapis.extensions.android.gms.auth.GooglePlayServicesAvailabilityIOException;
import com.google.api.client.googleapis.extensions.android.gms.auth.UserRecoverableAuthIOException;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.ExponentialBackOff;
import com.google.api.services.calendar.CalendarScopes;
import com.google.api.client.util.DateTime;
import com.google.api.services.calendar.model.*;
import com.google.api.services.drive.DriveScopes;
import android.Manifest;
import android.accounts.AccountManager;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.EasyPermissions;

import static android.content.ContentValues.TAG;

public class MainActivity extends Activity
        implements EasyPermissions.PermissionCallbacks
{
    GoogleAccountCredential mCredential;
    ProgressDialog mProgress;
    CalendarService mCalService;
    boolean mBound = false;
    boolean remoteBound = false;

    static final int REQUEST_ACCOUNT_PICKER = 1000;
    static final int REQUEST_AUTHORIZATION = 1001;
    static final int REQUEST_GOOGLE_PLAY_SERVICES = 1002;
    static final int REQUEST_PERMISSION_GET_ACCOUNTS = 1003;
    private EventAdapter eventAdapter;
    private static final String BUTTON_TEXT = "Update events";
    private static final String PREF_ACCOUNT_NAME = "accountName";
    private static final String[] SCOPES = {CalendarScopes.CALENDAR, DriveScopes.DRIVE};
    private Button bt;
    private TextView lbl;
    private Messenger mServiceMessenger;

    /**
     * Create the main activity.
     *
     * @param savedInstanceState previously saved instance data.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        lbl = new TextView(this);
        super.onCreate(savedInstanceState);
        LinearLayout activityLayout = new LinearLayout(this);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        activityLayout.setLayoutParams(lp);
        activityLayout.setOrientation(LinearLayout.VERTICAL);
        activityLayout.setPadding(16, 16, 16, 16);

        isStoragePermissionGranted();
        ListView eventList = new ListView(this);

        mCredential = GoogleAccountCredential.usingOAuth2(
                getApplicationContext(), Arrays.asList(SCOPES))
                .setBackOff(new ExponentialBackOff());

        eventAdapter = new EventAdapter(getApplicationContext(), mCredential);
        eventList.setAdapter(eventAdapter);

        bt = new Button(this);
        bt.setText("Bind to service");

        bt.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if(remoteBound)
                {
                    countEvents();
                }
                else
                {
                    bind();
                }
            }
        });

        EditText et =new EditText(this);

        et.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
                // When user changed the Text
                MainActivity.this.eventAdapter.getFilter().filter(cs);
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) { }

            @Override
            public void afterTextChanged(Editable arg0) {}
        });

        eventList.addHeaderView(bt);
        lbl.setText("");
        eventList.addHeaderView(lbl);
        eventList.addHeaderView(et);

        mProgress = new ProgressDialog(this);
        mProgress.setMessage("Getting events");
        Log.v("", "start");
        setContentView(eventList);
    }

    public void bind()
    {
        Intent intent = new Intent("com.example.ronan.remoteboundservice.RemoteService");
        intent.setPackage("com.example.ronan.remoteboundservice");
        bindService(intent,remoteConnection, Context.BIND_AUTO_CREATE);
    }

    public int countEvents()
    {
        Bundle bundle = new Bundle();
        bundle.putInt("key", this.eventAdapter.getCalEvents().size());
        Message message = Message.obtain();
        message.replyTo = new Messenger(handler);
        message.setData(bundle);
        try
        {
            mServiceMessenger.send(message);
        }
        catch (RemoteException e)
        {
            e.printStackTrace();
        }


        return  0;
    }

    @Override
    protected void onStart()
    {
        super.onStart();
        Log.v("", "start");
        Intent intent = new Intent(this, CalendarService.class);
        bindService(intent, mConnection, BIND_AUTO_CREATE);
        getResultsFromApi();
    }

    protected void onStop()
    {
        super.onStop();
        Log.v("end", "end");
        if (mBound)
        {
            unbindService(mConnection);
            mBound = false;
        }
    }


    public boolean isStoragePermissionGranted()
    {
        if (Build.VERSION.SDK_INT >= 23)
        {
            if (checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED)
            {
                Log.v(TAG, "Permission is granted");
                return true;
            } else
            {

                Log.v(TAG, "Permission is revoked");
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                return false;
            }
        } else
        { //permission is automatically granted on sdk<23 upon installation
            Log.v(TAG, "Permission is granted");
            return true;
        }
    }


    private void getResultsFromApi()
    {
        if (!isGooglePlayServicesAvailable())
        {
            acquireGooglePlayServices();
        } else if (mCredential.getSelectedAccountName() == null)
        {
            chooseAccount();
        } else if (!isDeviceOnline())
        {
            //device is offline
        } else
        {
            new MakeRequestTask(mCredential).execute();
        }
    }


    @AfterPermissionGranted(REQUEST_PERMISSION_GET_ACCOUNTS)
    private void chooseAccount()
    {
        if (EasyPermissions.hasPermissions(
                this, Manifest.permission.GET_ACCOUNTS))
        {
            String accountName = getPreferences(Context.MODE_PRIVATE)
                    .getString(PREF_ACCOUNT_NAME, null);
            if (accountName != null)
            {
                mCredential.setSelectedAccountName(accountName);
                getResultsFromApi();
            } else
            {
                // Start a dialog from which the user can choose an account
                startActivityForResult(
                        mCredential.newChooseAccountIntent(),
                        REQUEST_ACCOUNT_PICKER);
            }
        } else
        {
            // Request the GET_ACCOUNTS permission via a user dialog
            EasyPermissions.requestPermissions(
                    this,
                    "This app needs to access your Google account (via Contacts).",
                    REQUEST_PERMISSION_GET_ACCOUNTS,
                    Manifest.permission.GET_ACCOUNTS);
        }
    }

    Handler handler = new IncomingHandler();
    class IncomingHandler extends Handler
    {

        @Override
        public void handleMessage(Message msg)
        {
            Bundle data = msg.getData();
            String reply = data.getString("reply");
            lbl.setText(reply);

        }
    }


    @Override
    protected void onActivityResult(
            int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode)
        {
            case REQUEST_GOOGLE_PLAY_SERVICES:
                if (resultCode != RESULT_OK)
                {
                    //requires googleservices
                } else
                {
                    getResultsFromApi();
                }
                break;
            case REQUEST_ACCOUNT_PICKER:
                if (resultCode == RESULT_OK && data != null &&
                        data.getExtras() != null)
                {
                    String accountName =
                            data.getStringExtra(AccountManager.KEY_ACCOUNT_NAME);
                    if (accountName != null)
                    {
                        SharedPreferences settings =
                                getPreferences(Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = settings.edit();
                        editor.putString(PREF_ACCOUNT_NAME, accountName);
                        editor.apply();
                        mCredential.setSelectedAccountName(accountName);
                        getResultsFromApi();
                    }
                }
                break;
            case REQUEST_AUTHORIZATION:
                if (resultCode == RESULT_OK)
                {
                    getResultsFromApi();
                }
                break;
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String[] permissions,
                                           @NonNull int[] grantResults)
    {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        EasyPermissions.onRequestPermissionsResult(
                requestCode, permissions, grantResults, this);
    }


    @Override
    public void onPermissionsGranted(int requestCode, List<String> list)
    {
        // Do nothing.
    }

    @Override
    public void onPermissionsDenied(int requestCode, List<String> list)
    {
        // Do nothing.
    }

    private boolean isDeviceOnline()
    {
        ConnectivityManager connMgr =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        return (networkInfo != null && networkInfo.isConnected());
    }

    private boolean isGooglePlayServicesAvailable()
    {
        GoogleApiAvailability apiAvailability =
                GoogleApiAvailability.getInstance();
        final int connectionStatusCode =
                apiAvailability.isGooglePlayServicesAvailable(this);
        return connectionStatusCode == ConnectionResult.SUCCESS;
    }


    private void acquireGooglePlayServices()
    {
        GoogleApiAvailability apiAvailability =
                GoogleApiAvailability.getInstance();
        final int connectionStatusCode =
                apiAvailability.isGooglePlayServicesAvailable(this);
        if (apiAvailability.isUserResolvableError(connectionStatusCode))
        {
            showGooglePlayServicesAvailabilityErrorDialog(connectionStatusCode);
        }
    }


    void showGooglePlayServicesAvailabilityErrorDialog(
            final int connectionStatusCode)
    {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        Dialog dialog = apiAvailability.getErrorDialog(
                MainActivity.this,
                connectionStatusCode,
                REQUEST_GOOGLE_PLAY_SERVICES);
        dialog.show();
    }

    private ServiceConnection remoteConnection = new ServiceConnection()
    {

        @Override
        public void onServiceConnected(ComponentName className,
                                       IBinder service)
        {
            remoteBound = true;
            bt.setText("Count events remotely");
            mServiceMessenger = new Messenger(service);
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0)
        {
            remoteBound = false;
        }
    };


    private ServiceConnection mConnection = new ServiceConnection()
    {

        @Override
        public void onServiceConnected(ComponentName className,
                                       IBinder service)
        {
            CalendarService.LocalBinder binder = (CalendarService.LocalBinder) service;

            mCalService = binder.getService();
            binder.setvariables(eventAdapter);
            mBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0)
        {
            mBound = false;
        }
    };

    public class MakeRequestTask extends AsyncTask<Void, Void, List<CalEvent>>
    {
    private com.google.api.services.calendar.Calendar mService = null;
    private Exception mLastError = null;

    public MakeRequestTask(GoogleAccountCredential credential)
    {
        HttpTransport transport = AndroidHttp.newCompatibleTransport();
        JsonFactory jsonFactory = JacksonFactory.getDefaultInstance();
        mService = new com.google.api.services.calendar.Calendar.Builder(
                transport, jsonFactory, credential)
                .setApplicationName("Google Calendar API Android Quickstart")
                .build();
    }


    @Override
    protected List<CalEvent> doInBackground(Void... params)
    {
        try
        {
            return getDataFromApi();
        } catch (Exception e)
        {
            mLastError = e;
            cancel(true);
            return null;
        }
    }


    private List<CalEvent> getDataFromApi() throws IOException
    {
        DateTime now = new DateTime(System.currentTimeMillis());
        List<CalEvent> parsedEvents = new ArrayList<>();
        Events events = mService.events().list("primary")
                .setMaxResults(10)
                .setTimeMin(now)
                .setOrderBy("startTime")
                .setSingleEvents(true)
                .execute();

        List<Event> items = events.getItems();

        for (Event event : items)
        {
            DateTime start = event.getStart().getDateTime();
            CalEvent parsedEvent;
            if (start == null)
            {
                start = event.getStart().getDate();
            }
            try
            {
                 parsedEvent = new CalEvent(event.getAttachments().get(0).getFileUrl(), event.getSummary(), start, event.getId());
            }
            catch(Exception e)
            {
                 parsedEvent = new CalEvent(null, event.getSummary(), start, event.getId());
            }
            if (!eventAdapter.Exists(parsedEvent))
            {
                parsedEvents.add(parsedEvent);
            }
        }

        return parsedEvents;
    }


    @Override
    protected void onPreExecute()
    {
        mProgress.show();
    }

    @Override
    protected void onPostExecute(List<CalEvent> output)
    {
        mProgress.hide();
        if (output == null || output.size() == 0)
        {
        } else
        {
            eventAdapter.addItems(output);
        }
    }

    @Override
    protected void onCancelled()
    {
        mProgress.hide();
        if (mLastError != null)
        {
            if (mLastError instanceof GooglePlayServicesAvailabilityIOException)
            {
                showGooglePlayServicesAvailabilityErrorDialog(
                        ((GooglePlayServicesAvailabilityIOException) mLastError)
                                .getConnectionStatusCode());
            } else if (mLastError instanceof UserRecoverableAuthIOException)
            {
                startActivityForResult(
                        ((UserRecoverableAuthIOException) mLastError).getIntent(),
                        MainActivity.REQUEST_AUTHORIZATION);
            }
        }

    }
}
}