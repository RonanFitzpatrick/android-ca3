package com.android.ca3.roster;

import android.app.Service;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.util.Log;
import android.widget.Toast;

import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential;
import com.google.api.client.googleapis.extensions.android.gms.auth.GooglePlayServicesAvailabilityIOException;
import com.google.api.client.googleapis.extensions.android.gms.auth.UserRecoverableAuthIOException;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.DateTime;
import com.google.api.services.calendar.model.Event;
import com.google.api.services.calendar.model.Events;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class CalendarService extends Service
{
    private final IBinder mBinder = new LocalBinder();
    private static Timer timer = new Timer();
    public static EventAdapter adapter = null;

    public class LocalBinder extends Binder
    {
        CalendarService getService()
        {
            return CalendarService.this;
        }

        void setvariables(EventAdapter adapter)
        {
           CalendarService.adapter = adapter;
        }
    }

    @Override
    public IBinder onBind(Intent intent)
    {
        return mBinder;
    }

    public void onCreate()
    {
        super.onCreate();
        startService();
    }

    private void startService()
    {
        timer.scheduleAtFixedRate(new mainTask(), 0, 10000);
    }

    int before;

    private final Handler googleCallHandler = new Handler()
    {
        @Override
        public void handleMessage(Message msg)
        {
            if(adapter != null)
            {
                before = adapter.getCalEvents().size();
                new MakeRequestTask(adapter.credential).execute();
                adapter.update();

            }
        }
    };

    private class mainTask extends TimerTask
    {
        public void run()
        {
            Log.v("service", "Service");
            googleCallHandler.sendEmptyMessage(0);
        }
    }

    private class MakeRequestTask extends AsyncTask<Void, Void, List<CalEvent>>
    {
        private com.google.api.services.calendar.Calendar mService = null;
        private Exception mLastError = null;

        public MakeRequestTask(GoogleAccountCredential credential)
        {
            HttpTransport transport = AndroidHttp.newCompatibleTransport();
            JsonFactory jsonFactory = JacksonFactory.getDefaultInstance();
            mService = new com.google.api.services.calendar.Calendar.Builder(
                    transport, jsonFactory, credential)
                    .setApplicationName("Google Calendar API Android Quickstart")
                    .build();
        }


        @Override
        protected List<CalEvent> doInBackground(Void... params)
        {
            try
            {
                return getDataFromApi();
            } catch (Exception e)
            {
                mLastError = e;
                cancel(true);
                return null;
            }
        }


        private List<CalEvent> getDataFromApi() throws IOException
        {
            DateTime now = new DateTime(System.currentTimeMillis());
            List<CalEvent> parsedEvents = new ArrayList<>();
            Events events = mService.events().list("primary")
                    .setMaxResults(10)
                    .setTimeMin(now)
                    .setOrderBy("startTime")
                    .setSingleEvents(true)
                    .execute();

            List<Event> items = events.getItems();

            for (Event event : items)
            {
                DateTime start = event.getStart().getDateTime();
                CalEvent parsedEvent;
                if (start == null)
                {
                    start = event.getStart().getDate();
                }
                try
                {
                    parsedEvent = new CalEvent(event.getAttachments().get(0).getFileUrl(), event.getSummary(), start, event.getId());
                }
                catch (Exception e)
                {
                    parsedEvent = new CalEvent(null, event.getSummary(), start, event.getId());
                }
                if (!adapter.Exists(parsedEvent))
                {
                    parsedEvents.add(parsedEvent);
                }
            }

            return parsedEvents;
        }

        @Override
        protected void onPreExecute()
        {

        }

        @Override
        protected void onPostExecute(List<CalEvent> output)
        {

            if (output == null || output.size() == 0)
            {
            } else
            {
                adapter.addItems(output);
                int after = adapter.getCalEvents().size();
                if(after > before )
                {
                    Toast.makeText(getApplicationContext(),
                            after-before + " new events added", Toast.LENGTH_SHORT).show();
                }
            }
        }

        @Override
        protected void onCancelled()
        {

            if (mLastError != null)
            {
                if (mLastError instanceof GooglePlayServicesAvailabilityIOException)
                {

                } else if (mLastError instanceof UserRecoverableAuthIOException)
                {

                }
            }
        }
    }
}
