package com.android.ca3.roster;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;
import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import static android.content.ContentValues.TAG;


/**
 * Created by Ronan on 07/12/2016.
 */


public class EventAdapter extends BaseAdapter implements Filterable
{
    private List<CalEvent> calEvents = new ArrayList<CalEvent>();
    private Context context;

    public List<CalEvent> getCalEvents()
    {
        return calEvents;
    }

    public GoogleAccountCredential credential;

    public EventAdapter(Context context, GoogleAccountCredential credential)
    {
        this.context = context;
        this.credential = credential;
    }

    @Override
    public int getCount()
    {
        return calEvents.size();
    }

    public void addItems(List<CalEvent> events)
    {
        for (CalEvent e : events)
        {
            this.calEvents.add(e);
        }
        this.notifyDataSetChanged();
    }

    public boolean Exists(CalEvent check)
    {
        for (CalEvent e : calEvents)
        {
            if (check.getId().equals(e.getId()))
            {
                return true;
            }
        }
        return false;
    }

    @Override
    public Object getItem(int position)
    {
        return calEvents.get(position);
    }

    @Override
    public long getItemId(int position)
    {
        return position;
    }

    @Override
    public View getView(int position, final View convertView, final ViewGroup parent)
    {
        final CalEvent event = (CalEvent) this.getItem(position);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View eventLayout = inflater.inflate(R.layout.event, parent, false);
        Button url = (Button) eventLayout.findViewById(R.id.link);

        if(event.getAttachmentUrl() != null)
        {

            url.setText("download attachment");
            url.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View view)
                {
                    new Download(credential, event.getAttachmentUrl()).execute();
                }
            });
        }
        else
        {
            url.setVisibility(View.INVISIBLE);
        }

        TextView name = (TextView) eventLayout.findViewById(R.id.name);
        name.setText(event.getEventName());

        TextView date = (TextView) eventLayout.findViewById(R.id.date);
        date.setText(event.getDate().toString());


        eventLayout.setOnLongClickListener(new View.OnLongClickListener()
        {
            @Override
            public boolean onLongClick(View v)
            {
                ArrayList toRemove = new ArrayList();
                for (CalEvent listevent : calEvents)
                {
                    if (listevent.getId().equals(event.getId()))
                    {
                        toRemove.add(listevent);

                    }
                }
                calEvents.removeAll(toRemove);
                new Delete(event.getId()).execute();
                notifyDataSetChanged();
                return true;

            }
        });

        return eventLayout;
    }

    public void update()
    {
        notifyDataSetChanged();
    }

    @Override
    public Filter getFilter()
    {
        Filter filter = new Filter() {

            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence constraint, FilterResults results)
            {

                calEvents = (List<CalEvent>) results.values;
                notifyDataSetChanged();
            }

            @Override
            protected FilterResults performFiltering(CharSequence constraint)
            {

                FilterResults results = new FilterResults();
                ArrayList<CalEvent> FilteredArrayNames = new ArrayList<CalEvent>();

                // perform your search here using the searchConstraint String.

                constraint = constraint.toString().toLowerCase();
                for (CalEvent event : calEvents)
                {
                    if (event.getEventName().toLowerCase().startsWith(constraint.toString()))
                    {
                        FilteredArrayNames.add(event);
                    }
                }

                results.count = FilteredArrayNames.size();
                results.values = FilteredArrayNames;
                Log.e("VALUES", results.values.toString());

                return results;
            }
        };

        return filter;
    }

    private class Delete extends AsyncTask<Void, Void, Boolean>
    {
        private com.google.api.services.calendar.Calendar mService = null;
        private String id;

        public Delete(String id)
        {
            HttpTransport transport = AndroidHttp.newCompatibleTransport();
            JsonFactory jsonFactory = JacksonFactory.getDefaultInstance();
            mService = new com.google.api.services.calendar.Calendar.Builder(
                    transport, jsonFactory, credential)
                    .setApplicationName("Google Calendar API Android Quickstart")
                    .build();
            this.id = id;
        }

        @Override
        protected Boolean doInBackground(Void... params)
        {
            try
            {
                mService.events().delete("primary", id).execute();
            } catch (IOException e)
            {
                e.printStackTrace();
            }
            return true;
        }

    }


    private class Download extends AsyncTask<Void, Void, Boolean>
    {
        private com.google.api.services.drive.Drive mService = null;
        private String id;

        Download(GoogleAccountCredential credential, String id)
        {
            HttpTransport transport = AndroidHttp.newCompatibleTransport();
            JsonFactory jsonFactory = JacksonFactory.getDefaultInstance();
            mService = new com.google.api.services.drive.Drive.Builder(
                    transport, jsonFactory, credential)
                    .setApplicationName("Drive API Android Quickstart")
                    .build();
            this.id = id;

        }


        @Override
        protected Boolean doInBackground(Void... params)
        {
            try
            {
                getDataFromApi(id);
                return true;
            } catch (Exception e)
            {
                cancel(true);
                return null;
            }
        }

        /**
         * Fetch a list of up to 10 file names and IDs.
         *
         * @return List of Strings describing files, or an empty list if no files
         * found.
         * @throws IOException
         */
        private void getDataFromApi(String id) throws IOException
        {

            if (context.checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED)
            {
                Log.v(TAG, "Permission is granted");
            }

            File file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS), "HelloWorld.txt");
            file.createNewFile();
            OutputStream outputStream = new FileOutputStream(file);
            mService.files().export(id, "text/plain").executeMediaAndDownloadTo(outputStream);
            outputStream.flush();
            outputStream.close();
        }


    }

}
